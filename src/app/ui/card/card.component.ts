import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  title: string = 'Sou outro titulo';
  link: string = 'https://google.com';
  showCard: boolean = false;
  constructor() {
   }

  ngOnInit() {
  }

  updateLink(): void {
    this.link = 'https://facebook.com';
  }

}
