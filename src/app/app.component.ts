import { Component } from '@angular/core';

@Component({
  selector: 'app-cafe',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'exemplo-inicial-angular';
}
