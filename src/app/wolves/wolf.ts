export class Wolf {
    constructor(
        public name: string,
        public age: number,
        public description: string,
        public photo: string      
    ){}
}

/*
gitlab.com/pacotrim/lobinhos-api
*/
