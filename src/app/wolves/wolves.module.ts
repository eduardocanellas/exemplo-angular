import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WolfService } from './wolf.service';
import { WolfListComponent } from './wolf-list/wolf-list.component';
import { WolfShowComponent } from './wolf-show/wolf-show.component';
import { RouterModule, Route } from '@angular/router';

const routes: Route[] = [
  {
    path: 'lobos/:id', component: WolfShowComponent
  },
  {
    path: 'lobos', component: WolfListComponent
  },
];

@NgModule({
  declarations: [
    WolfListComponent,
    WolfShowComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    WolfService
  ],
  exports: [
    WolfListComponent
  ]
})
export class WolvesModule { }
