import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { WolfService } from '../wolf.service';
import { switchMap } from 'rxjs/operators';
import { Wolf } from '../wolf';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-wolf-show',
  templateUrl: './wolf-show.component.html',
  styleUrls: ['./wolf-show.component.scss']
})
export class WolfShowComponent implements OnInit {
  wolf$;
  constructor(
    private route: ActivatedRoute,
    private wolfService: WolfService
  ) { }

  ngOnInit() {
    this.wolf$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        return this.wolfService.getWolf(+params.get('id'));
      })
    );
  }

}
