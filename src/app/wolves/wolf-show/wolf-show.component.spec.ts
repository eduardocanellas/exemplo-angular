import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WolfShowComponent } from './wolf-show.component';

describe('WolfShowComponent', () => {
  let component: WolfShowComponent;
  let fixture: ComponentFixture<WolfShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WolfShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WolfShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
