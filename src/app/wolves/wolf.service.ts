import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Wolf } from './wolf';


@Injectable()
export class WolfService {
  baseUrl = 'https://lobinhos-api.herokuapp.com/';

  constructor(
    private http: HttpClient
  ) { }

  getWolves() {
    return this.http.get(this.baseUrl + 'wolves');
  }

  getWolf(id: number){
    return this.http.get(this.baseUrl + `wolves/${id}`);
  }
}
