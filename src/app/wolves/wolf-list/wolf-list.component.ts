import { Component, OnInit } from '@angular/core';
import { WolfService } from '../wolf.service';
import { Wolf } from '../wolf';
import { Router } from '@angular/router';

@Component({
  selector: 'app-wolf-list',
  templateUrl: './wolf-list.component.html',
  styleUrls: ['./wolf-list.component.scss']
})
export class WolfListComponent implements OnInit {
  wolves = [];

  constructor(
    private wolfService: WolfService,
    private router: Router
  ) {  }

  ngOnInit() {
    this.wolfService.getWolves().subscribe(
      (data: []) => { 
        this.wolves = data.map(info => new Wolf(
          info['name'],
          +info['age'],
          info['description'],
          info['photo']
        ));
       }
    );
  }

  goToShow(id: number){
    this.router.navigate(['/lobos', id]);
  }
}
